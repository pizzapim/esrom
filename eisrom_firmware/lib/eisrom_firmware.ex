defmodule EisromFirmware do
  @moduledoc """
  Documentation for EisromFirmware.
  """

  @doc """
  Hello world.

  ## Examples

      iex> EisromFirmware.hello
      :world

  """
  def hello do
    :world
  end
end
