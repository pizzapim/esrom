defmodule EisromUiWeb.MorseLive do
  use Phoenix.LiveView
  require Logger

  @topic "morse_progress"

  def render(assigns) do
    EisromUiWeb.PageView.render("morse.html", assigns)
  end

  def mount(_params, _session, socket) do
    EisromUiWeb.Endpoint.subscribe(@topic)
    {:ok, assign(socket, default_assigns())}
  end

  def handle_event("toggle_morse", _value, socket) do
    Logger.info("Button pressed!")

    Eisrom.Morse.Server.toggle_morse()

    try do
      Task.async(fn -> post_telegram_message() end)
    rescue
      e in RuntimeError -> e
    end

    {:noreply, socket}
  end

  def handle_event("toggle_hint", _value, socket) do
    {:noreply, update(socket, :hints_visible, &(not &1))}
  end

  def handle_info(progress, socket) do
    {:noreply, assign(socket, progress: progress, in_progress?: Eisrom.Morse.Server.in_progress?())}
  end

  defp default_assigns do
    [
      progress: Eisrom.Morse.Server.progress(),
      in_progress?: Eisrom.Morse.Server.in_progress?(),
      hints_visible: false
    ]
  end

  defp post_telegram_message do
    Tesla.post("https://api.telegram.org/bot#{telegram_bot_token()}/sendMessage?text=PUSHED&chat_id=#{telegram_chat_id()}", "")
  end

  defp telegram_bot_token do
    Application.fetch_env!(:eisrom_ui, :telegram_bot_token)
  end

  defp telegram_chat_id do
    Application.fetch_env!(:eisrom_ui, :telegram_chat_id)
  end
end
