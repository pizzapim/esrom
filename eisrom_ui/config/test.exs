import Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :eisrom_ui, EisromUiWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "dfppXvxP1vXJhlk0z7H3jC/wqrQw/SUXVXG9RmTPIHPHSqBh/L8KllPViDdiEfay",
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime
