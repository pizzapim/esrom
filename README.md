# Esrom geocache

## Development
- `cd eisrom_ui`
- `mix phx.server`

## Deployment
- `cd eisrom_ui`
- `export MIX_TARGET=host`
- `export MIX_ENV=prod`
- `mix deps.get`
- `mix assets.deploy`
- `cd ../eisrom_firmware`
- `export MIX_TARGET=rpi2`
- `mix deps.get`
- `mix firmware`
- `mix firmware.burn`

