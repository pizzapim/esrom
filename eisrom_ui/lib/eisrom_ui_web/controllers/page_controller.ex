defmodule EisromUiWeb.PageController do
  use EisromUiWeb, :controller

  def index(conn, _params) do
    send_resp(conn, 204, "")
  end

  def instructions(conn, _params) do
    render(conn, :instructions)
  end

  def morse(conn, _params) do
    Phoenix.LiveView.Controller.live_render(conn, EisromUiWeb.MorseLive)
  end
end
